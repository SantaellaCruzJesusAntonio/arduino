#include <SPI.h>  //Importamos librería comunicación SPI
#include <Ethernet.h>  //Importamos librería Ethernet
#include <dht.h>
dht DHT;
#define DHT11_PIN 2
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };//Ponemos la dirección MAC de la Ethernet Shield que está con una etiqueta debajo la placa
IPAddress ip(192,168,1,170); //Asingamos la IP al Arduino
EthernetServer server(80); //Creamos un servidor Web con el puerto 80 que es el puerto HTTP por defecto
//relay pin A0 error
//relay pin A1 //relay1
//relay pin A2 //relay2
//relay pin A3 //relay3
 byte temperatura=0;
 byte humedad=0;
byte estado=0; //Estado del Led inicialmente "OFF"
byte estado1=0; //Estado del Led inicialmente "OFF"
byte estado2=0; //Estado del Led inicialmente "OFF"
byte estado3=0;
void setup()
{ sensar(); 
  Ethernet.begin(mac, ip);// Inicializamos la comunicación Ethernet y el servidor
  server.begin();
  pinMode(A1,OUTPUT);
  pinMode(A2,OUTPUT);
  pinMode(A3,OUTPUT);
  pinMode(A4,OUTPUT);
  pinMode(A0,OUTPUT);
}
void loop()
{  
  EthernetClient client = server.available(); //Creamos un cliente Web
  //Cuando detecte un cliente a través de una petición HTTP
  if (client) {
    boolean currentLineIsBlank = true; //Una petición HTTP acaba con una línea en blanco
    String cadena=""; //Creamos una cadena de caracteres vacía
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();//Leemos la petición HTTP carácter por carácter
        cadena.concat(c);//Unimos el String 'cadena' con la petición HTTP (c). De esta manera convertimos la petición HTTP a un String
          int posicion;
         //Ya que hemos convertido la petición HTTP a una cadena de caracteres, ahora podremos buscar partes del texto.
         if(cadena.indexOf("A=")>0)
          posicion=cadena.indexOf("A="); //Guardamos la posición de la instancia "LED=" a la variable 'posicion'
          else if (cadena.indexOf("B=")>0)
          posicion=cadena.indexOf("B=");
          else if (cadena.indexOf("C=")>0)
          posicion=cadena.indexOf("C=");
          /*else if(cadena.indexOf("D=")>0)
          posicion=cadena.indexOf("D=");*/
          if(cadena.substring(posicion)=="A=1")//Si a la posición 'posicion' hay "LED=ON"
          {
            digitalWrite(A1,HIGH);
            estado=1;
          }
          if(cadena.substring(posicion)=="A=0")//Si a la posición 'posicion' hay "LED=OFF"
          {
            digitalWrite(A1,LOW);
            estado=0;
          }
          if(cadena.substring(posicion)=="B=1")//Si a la posición 'posicion' hay "LED=ON"
          {
            digitalWrite(A2,HIGH);
            estado1=1;
          }
          if(cadena.substring(posicion)=="B=0")//Si a la posición 'posicion' hay "LED=OFF"
          {
            digitalWrite(A2,LOW);
            estado1=0;
          }
          if(cadena.substring(posicion)=="C=1")//Si a la posición 'posicion' hay "LED=ON"
          {
            digitalWrite(A3,HIGH);
            estado2=1;
          }
          if(cadena.substring(posicion)=="C=0")//Si a la posición 'posicion' hay "LED=OFF"
          {
            digitalWrite(A3,LOW);
            estado2=0;
          }
        //Cuando reciba una línea en blanco, quiere decir que la petición HTTP ha acabado y el servidor Web está listo para enviar una respuesta
        if (c == '\n' && currentLineIsBlank) {
            // Enviamos al cliente una respuesta HTTP
            client.println("HTTP/1.1 200 OK");
            client.println("Content-Type: text/html");
            client.println();
            //Página web en formato HTML
            client.println("<html><head></head>");
            client.println("<body>");
            client.println("<h1 align='center'>Los honguitos!!!</h1>");
            client.println("<div style='text-align:center;'>");
            if(estado==0)
              client.println("<button onClick=location.href='./?A=1\' style='margin:auto;background-color: #84B1FF;color: snow;padding: 10px;border: 1px solid #3F7CFF;width:65px;'>ON</button>");
            else
              client.println("<button onClick=location.href='./?A=0\' style='margin:auto;background-color: #84B1FF;color: snow;padding: 10px;border: 1px solid #3F7CFF;width:65px;'>0FF</button>");
            if(estado1==0)
              client.println("<button onClick=location.href='./?B=1\' style='margin:auto;background-color: #84B1FF;color: snow;padding: 10px;border: 1px solid #3F7CFF;width:65px;'>ON</button>");  
            else
              client.println("<button onClick=location.href='./?B=0\' style='margin:auto;background-color: #84B1FF;color: snow;padding: 10px;border: 1px solid #3F7CFF;width:65px;'>OFF</button>");
            if(estado2==0)
              client.println("<button onClick=location.href='./?C=1\' style='margin:auto;background-color: #84B1FF;color: snow;padding: 10px;border: 1px solid #3F7CFF;width:65px;'>ON</button>");
            else
              client.println("<button onClick=location.href='./?C=0\' style='margin:auto;background-color: #84B1FF;color: snow;padding: 10px;border: 1px solid #3F7CFF;width:65px;'>OFF</button>");
              if(estado3==0)
              client.println("<button onClick=location.href='./?D=1\' style='margin:auto;background-color: #84B1FF;color: snow;padding: 10px;border: 1px solid #3F7CFF;width:65px;'>ON</button>");
            else
              client.println("<button onClick=location.href='./?D=0\' style='margin:auto;background-color: #84B1FF;color: snow;padding: 10px;border: 1px solid #3F7CFF;width:65px;'>OFF</button>");
            if(temperatura>0){
            client.println("valor de Temperatura: ");
            client.print(temperatura);
            client.print("Min 0 C <progress max=\"50\" value=\"");
            client.print(temperatura);
            client.print("\" ></progress>Max 50 C");
            }
            client.println("</body></html>");
            break;
        }
        if (c == '\n') {
          currentLineIsBlank = true;
        }
        else if (c != '\r') {
          currentLineIsBlank = false;
        }
      }
    }
    delay(1);
    client.stop();// Cierra la conexión
  }  
}
void sensar(){
  int chk = DHT.read11(DHT11_PIN);
  if(DHT.temperature-4 <0){
  digitalWrite(A0,LOW);
  temperatura=0;
  humedad=0;
  }else{
   digitalWrite(A0,HIGH);
  temperatura=DHT.temperature-4;
  humedad=DHT.humidity;
  }
 }

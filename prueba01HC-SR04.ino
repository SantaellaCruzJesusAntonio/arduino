const int pinEcho =8;
const int pinTrigg=9;
const int pinLed=13;
unsigned int tiempo=0,distancia=0,error=3;
void setup() {
Serial.begin(9600);
pinMode(pinEcho,INPUT);
pinMode(pinTrigg,OUTPUT);
pinMode(13,OUTPUT);
}
void loop() {
 digitalWrite(pinTrigg,LOW);
 delayMicroseconds(2);
 digitalWrite(pinTrigg,HIGH);
 delayMicroseconds(10);
 digitalWrite(pinTrigg, LOW);
 tiempo = pulseIn(pinEcho, HIGH);6
 // LA VELOCIDAD DEL SONIDO ES DE 340 M/S O 29 MICROSEGUNDOS POR CENTIMETRO
  // DIVIDIMOS EL TIEMPO DEL PULSO ENTRE 58, TIEMPO QUE TARDA RECORRER IDA Y VUELTA UN CENTIMETRO LA ONDA SONORA
  distancia = (tiempo / 58)-error;
  
  // ENVIAR EL RESULTADO AL MONITOR SERIAL
  Serial.print(distancia);
  Serial.println(" cm");
  delay(200);
 // ENCENDER EL LED CUANDO SE CUMPLA CON CIERTA DISTANCIA
  if (distancia <= 15) {
    digitalWrite(13, HIGH);
    delay(500);
  } else {
    digitalWrite(13, LOW);
  }
 
}

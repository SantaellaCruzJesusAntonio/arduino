#include <SPI.h>
#include <Ethernet.h>
#include <dht.h>
dht DHT;
#define DHT11_PIN 2
#define ON   0
#define OFF  1
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };   // mac address
byte ip[] = { 192, 168, 1, 170 };                      // configurate il vostro IP a vostra scelta("192.168.1.89")
byte gateway[] = { 192, 168, 1, 1 };                   // internet access router
byte subnet[] = { 255, 255, 255, 0 };                  //subnet mask
byte temperatura=0;byte humedad=0;
EthernetServer server(80);                             //server port     
String readString;
//reelevadores3,4,5,6
void setup() {
  Serial.begin(9600);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  digitalWrite(3,OFF);
  digitalWrite(4,OFF);
  digitalWrite(5,OFF);
  digitalWrite(6,OFF);
  
  // Inizio la comunicazione Ethernet con il server:
  Ethernet.begin(mac, ip, gateway, subnet);
  server.begin();
  sensar();
}

void sensar(){
  int chk = DHT.read11(DHT11_PIN);
  if(DHT.temperature-4 <0){
  digitalWrite(A0,LOW);
  temperatura=0;
  humedad=0;
  }else{
   digitalWrite(A0,HIGH);
  temperatura=DHT.temperature-4;
  humedad=DHT.humidity;
  }
}
void loop() {
  EthernetClient client = server.available();
  if (client) {
    while (client.connected()) {   
      if (client.available()) {
        char c = client.read();
        if (readString.length() < 100) {
          readString += c;
         }
         if (c == '\n') {          
           Serial.println(readString); 
           client.println("HTTP/1.1 200 OK");
           client.println("Content-Type: text/html");
           client.println();     
           client.println("<HTML>");
           client.println("<HEAD>");
           //client.println("<link rel='stylesheet' type='text/css' href='http://www.progettiarduino.com/uploads/8/1/0/8/81088074/style3.css' />");
           client.println("<style>.enlaceboton{text-decoration: none;padding: 10px;font-weight: 600;font-size: 20px;color: #ffffff;background-color: #1883ba;border-radius: 6px;border: 2px solid #0016b0;}boton_personalizado:hover{color: #1883ba;background-color: #ffffff;}</style>");
           client.println("<TITLE>Somos honguitos!!!</TITLE>");
           client.println("</HEAD><BODY>");
           client.println("<H1>Controllo de Sistema</H1><hr /></br>");
           client.println("<br />");  
           client.println("<a href=\"/?button1on\"\" class=\"enlaceboton\">ON</a>");          
           client.println("<a href=\"/?button1off\"\" class=\"enlaceboton\">OFF</a>");    
           client.println("Temperatura<br /></br>");  
           client.println("<a href=\"/?button2on\"\" class=\"enlaceboton\">ON</a>");         
           client.println("<a href=\"/?button2off\"\" class=\"enlaceboton\">OFF</a>");    
           client.println("Humedad<br /></br>");   
           client.println("<a href=\"/?button3on\"\" class=\"enlaceboton\">ON</a>");         
           client.println("<a href=\"/?button3off\"\" class=\"enlaceboton\">OFF</a>");    
           client.println("Riego<br /></br>");   
           client.println("<a href=\"/?button4on\"\" class=\"enlaceboton\">ON</a>");         
           client.println("<a href=\"/?button4off\"\" class=\"enlaceboton\">OFF</a>");    
           client.println("Luz<br /></br>");  
           client.println("valor de Temperatura: ");
           client.print(temperatura);
           client.print("</br><progress max=\"30\" value=\"");
           client.print(temperatura);
           client.print("\" ></progress>Max 30 C");  
           client.println("</br>valor de Humedad: ");
           client.print(humedad);
           client.print("%</br><progress max=\"85\" value=\"");
           client.print(humedad);
           client.print("\" ></progress>Max 85%"); 
           client.println("</BODY>");
           client.println("</HTML>");
     
           delay(1);
           client.stop();
           //Controlli su Arduino: Se è stato premuto il pulsante sul webserver
           if (readString.indexOf("?button1on") >0){
               digitalWrite(3,ON);
           }
           if (readString.indexOf("?button1off") >0){
               digitalWrite(3,OFF);
           }
           if (readString.indexOf("?button2on") >0){
               digitalWrite(4, ON);  
           }
           if (readString.indexOf("?button2off") >0){
               digitalWrite(4, OFF);
           }
           if (readString.indexOf("?button3on") >0){
               digitalWrite(5, ON);  
           }
           if (readString.indexOf("?button3off") >0){
               digitalWrite(5, OFF);
           }
           if (readString.indexOf("?button4on") >0){
               digitalWrite(6, ON);  
           }
           if (readString.indexOf("?button4off") >0){
               digitalWrite(6, OFF);
           }
            readString="";  
           
         }
       }
    }
  
}
}


#include <Wire.h>
//MAESTRO
void setup()
{
  Wire.begin();        // Conexión al Bus I2C
  Serial.begin(9600);  // Velocidad de conexión
}

void loop()
{
  Wire.requestFrom(2, 1);    // Le pide 10 bytes al Esclavo 2

  while(Wire.available())    // slave may send less than requested
  { 
    unsigned int c = Wire.read();   // Recibe byte a byte
    Serial.print(c);        // Presenta los caracteres en el Serial Monitor
  }
    Serial.println();       // Cámbia de línea en el Serial Monitor.
    delay(500);
}

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(7,OUTPUT);
  pinMode(8,OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  encender(7);
  encender(8);
               
}
void encender(int pin){
  digitalWrite(pin, HIGH);   
  delay(1000);             
  digitalWrite(pin, LOW);   
 }

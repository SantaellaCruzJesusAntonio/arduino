#include <dht.h>
#include <SPI.h>  //Importamos librería comunicación SPI
#include <Ethernet.h>  //Importamos librería Ethernet
dht DHT;
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };//Ponemos la dirección MAC de la Ethernet Shield que está con una etiqueta debajo la placa
IPAddress ip(192,168,1,10); //Asingamos la IP al Arduino
EthernetServer server(80); //Creamos un servidor Web con el puerto 80 que es el puerto HTTP por defecto


#define DHT11_PIN 2s

void setup(){
  Serial.begin(9600);
  pinMode(3,OUTPUT);
  Ethernet.begin(mac, ip);
  server.begin();
}
void loop()
{
  int chk = DHT.read11(DHT11_PIN);
  if(DHT.temperature-4 > 0){
  digitalWrite(3,HIGH);
  Serial.print("Temperature = ");
  Serial.println(DHT.temperature-4);
  Serial.print("Humidity = ");
  Serial.println(DHT.humidity);
  delay(1000);
  }else{
    Serial.println(DHT.temperature-4);
    digitalWrite(3,LOW);
    }

  EthernetClient client = server.available(); //Creamos un cliente Web
  //Cuando detecte un cliente a través de una petición HTTP
  if (client) {
    Serial.println("new client");
    boolean currentLineIsBlank = true; //Una petición HTTP acaba con una línea en blanco
    String cadena=""; //Creamos una cadena de caracteres vacía
    while (client.connected()) {  
            // Enviamos al cliente una respuesta HTTP
            client.println("HTTP/1.1 200 OK");
            client.println("Content-Type: text/html");
            client.println();
 
            //Página web en formato HTML
            client.println("<html>");
            client.println("<head>");
            client.println("</head>");
            client.println("<body>");
            client.println("<h1 align='center'>Somos los honguitos</h1><h3 align='center'>Servidor Web con Arduino</h3>");
            //Creamos los botones. Para enviar parametres a través de HTML se utiliza el metodo URL encode. Los parámetros se envian a través del símbolo '?'
            client.println("<div style='text-align:center;'>");
            client.println("<p align=center> Temperatura:");
            client.println(DHT.temperature-4);
            client.println("<br>Maximo valor 50 ºC");
            client.print("</p><progress value=\"");
            client.print(DHT.temperature-4);
            client.print("\" max=\"50\" min=\"28\">");
            client.print(DHT.temperature-4);
            client.println("%</progress><br>");
            client.println("Humedad:");
            client.print(DHT.humidity);
            client.println("%");
            client.println("</body>");
            client.println("</html>");
            break;
       
      }
      client.stop();
    }
}

#include <SPI.h>
#include <Ethernet.h>
#include <dht.h>
dht DHT;
#define DHT11_PIN 2
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };   // direccion mac
byte ip[] = { 192, 168, 1, 170 };                      // configurate ip
byte gateway[] = { 192, 168, 1, 1 };                   // gateway
byte subnet[] = { 255, 255, 255, 0 };                  //mascara
byte temperatura=0;byte humedad=0;
unsigned long tiempo = 0,tmpAnterior = 0,tmpLuz = 0,tmpRiego = 0;
boolean edo1=false,edo2=false,edo3=false,edo4=false;
EthernetServer server(80);                             //puerto del servidor
String readString;

void setup() {
  Serial.begin(9600);
  pinMode(3, OUTPUT);//Temperatura
  pinMode(4, OUTPUT);//Humedad
  pinMode(5, OUTPUT);//Riego
  pinMode(6, OUTPUT);//Luz

  // inicia  la comunicacion  Ethernet con el servidor:
  Ethernet.begin(mac, ip, gateway, subnet);
  server.begin();
  sensar();
}

void sensar(){
  int chk = DHT.read11(DHT11_PIN);
  if(DHT.temperature-4 <0){
  digitalWrite(A0,LOW);
  temperatura=0;
  humedad=0;
  }else{
   digitalWrite(A0,HIGH);
  temperatura=DHT.temperature-4;
  humedad=DHT.humidity;
  }
}
void loop() {
  tiempo=millis();
  if(tiempo - tmpRiego > 14000){
    //señal HIGH al rele del riego x goteo
    digitalWrite(5, HIGH);
  }
  if(tiempo - tmpLuz > 16000){
    //señal HIGH al rele de la luz 
    digitalWrite(6, HIGH);
  }
  if(tiempo - tmpLuz > 24000 && tiempo - tmpRiego > 24000){
      //Señal LOW al rele de luz y de riego
      digitalWrite(5, LOW);
      digitalWrite(6, LOW);
      tmpLuz = tiempo;
      tmpRiego = tiempo;
  }//botones
    if(analogRead(A0)<50){
    digitalWrite(3,!edo1);
    edo1=!edo1;
    }
    if(analogRead(A1)<50){
    digitalWrite(4,!edo2);
    edo2=!edo2;
    }
    if(analogRead(A2)<50){
    digitalWrite(5,!edo3);
    edo3=!edo3;
    }
    if(analogRead(A3)<50){
    digitalWrite(6,!edo4);
    edo4=!edo4;
    }
  EthernetClient client = server.available();
  if (client) {
    while (client.connected()) {   
      if (client.available()) {
        char c = client.read();
        if (readString.length() < 100) {
          readString += c;
         }
         if (c == '\n') {          
           Serial.println(readString); 
           client.println("HTTP/1.1 200 OK");
           client.println("Content-Type: text/html");
           client.println();     
           client.println("<HTML>");
           client.println("<HEAD>");
           //client.println("<link rel='stylesheet' type='text/css' href='http://www.progettiarduino.com/uploads/8/1/0/8/81088074/style3.css' />");
           client.println("<style>.enlaceboton{text-decoration: none;padding: 10px;font-weight: 600;font-size: 20px;color: #ffffff;background-color: #1883ba;border-radius: 6px;border: 2px solid #0016b0;}boton_personalizado:hover{color: #1883ba;background-color: #ffffff;}</style>");
           client.println("<TITLE>Somos honguitos!!!</TITLE>");
           client.println("</HEAD><BODY>");
           client.println("<H1>Controllo de Sistema</H1><hr /></br>");
           client.println("<br />");  
           client.println("<a href=\"/?button1on\"\" class=\"enlaceboton\">ON</a>");          
           client.println("<a href=\"/?button1off\"\" class=\"enlaceboton\">OFF</a>");    
           client.println("Temperatura<br /></br>");  
           client.println("<a href=\"/?button2on\"\" class=\"enlaceboton\">ON</a>");         
           client.println("<a href=\"/?button2off\"\" class=\"enlaceboton\">OFF</a>");    
           client.println("Humedad<br /></br>");   
           client.println("<a href=\"/?button3on\"\" class=\"enlaceboton\">ON</a>");         
           client.println("<a href=\"/?button3off\"\" class=\"enlaceboton\">OFF</a>");    
           client.println("Riego<br /></br>");   
           client.println("<a href=\"/?button4on\"\" class=\"enlaceboton\">ON</a>");         
           client.println("<a href=\"/?button4off\"\" class=\"enlaceboton\">OFF</a>");    
           client.println("Luz<br /><br>");  
           client.println("valor de Temperatura: ");
           client.print(temperatura);
           client.print("</br><progress max=\"30\" value=\"");
           client.print(temperatura);
           client.print("\" ></progress>Max 30 C");  
           client.println("</br>valor de Humedad: ");
           client.print(humedad);
           client.print("%</br><progress max=\"85\" value=\"");
           client.print(humedad);
           client.print("\" ></progress>Max 85%"); 
           client.println("</BODY>");
           client.println("</HTML>");
     
           delay(1);
           client.stop();
           //control desde el web service
           if (readString.indexOf("?button1on") >0){
               digitalWrite(3, LOW);
           }
           if (readString.indexOf("?button1off") >0){
               digitalWrite(3, HIGH);
           }
           if (readString.indexOf("?button2on") >0){
               digitalWrite(4, LOW);  
           }
           if (readString.indexOf("?button2off") >0){
               digitalWrite(4, HIGH);
           }
           if (readString.indexOf("?button3on") >0){
               digitalWrite(5, LOW);  
           }
           if (readString.indexOf("?button3off") >0){
               digitalWrite(5, HIGH);
           }
           if (readString.indexOf("?button4on") >0){
               digitalWrite(6, LOW);  
           }
           if (readString.indexOf("?button4off") >0){
               digitalWrite(6, HIGH);
           }
            readString="";  
           
         }
       }
    }
  
}
sensar();
}


#include <SPI.h>  //Importamos librería comunicación SPI
#include <Ethernet.h>  //Importamos librería Ethernet
#include <dht.h>
dht DHT;
#define DHT11_PIN 2
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };//Ponemos la dirección MAC de la Ethernet Shield que está con una etiqueta debajo la placa
IPAddress ip(192,168,1,10); //Asingamos la IP al Arduino
EthernetServer server(80); //Creamos un servidor Web con el puerto 80 que es el puerto HTTP por defecto
 //relay pin 0
 //relay pin 1
 //relay pin 2
 //relay pin 3
byte led=A2; //Pin del led
byte led1=A1;
 byte temperatura=0;
 byte humedad=0;
String estado="OFF"; //Estado del Led inicialmente "OFF"
String estado1="OFF"; //Estado del Led inicialmente "OFF"
 
void setup()
{
  sensar();
  Serial.begin(9600);
 
  // Inicializamos la comunicación Ethernet y el servidor
  Ethernet.begin(mac, ip);
  server.begin();
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());
 
  pinMode(led,OUTPUT);
  pinMode(led1,OUTPUT);
  pinMode(A0,OUTPUT);
}
 
void loop()
{  
  EthernetClient client = server.available(); //Creamos un cliente Web
  //Cuando detecte un cliente a través de una petición HTTP
  if (client) {
    boolean currentLineIsBlank = true; //Una petición HTTP acaba con una línea en blanco
    String cadena=""; //Creamos una cadena de caracteres vacía
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();//Leemos la petición HTTP carácter por carácter
        cadena.concat(c);//Unimos el String 'cadena' con la petición HTTP (c). De esta manera convertimos la petición HTTP a un String
          int posicion;
         //Ya que hemos convertido la petición HTTP a una cadena de caracteres, ahora podremos buscar partes del texto.
         if(cadena.indexOf("LED=")>0)
          posicion=cadena.indexOf("LED="); //Guardamos la posición de la instancia "LED=" a la variable 'posicion'
          else if (cadena.indexOf("prende=")>0)
          posicion=cadena.indexOf("prende=");
          if(cadena.substring(posicion)=="LED=ON")//Si a la posición 'posicion' hay "LED=ON"
          {
            digitalWrite(led,HIGH);
            estado="ON";
          }
          if(cadena.substring(posicion)=="LED=OFF")//Si a la posición 'posicion' hay "LED=OFF"
          {
            digitalWrite(led,LOW);
            estado="OFF";
          }
          if(cadena.substring(posicion)=="prende=ON")//Si a la posición 'posicion' hay "LED=ON"
          {
            digitalWrite(led1,HIGH);
            estado1="ON";
          }
          if(cadena.substring(posicion)=="prende=OFF")//Si a la posición 'posicion' hay "LED=OFF"
          {
            digitalWrite(led1,LOW);
            estado1="OFF";
          }
        //Cuando reciba una línea en blanco, quiere decir que la petición HTTP ha acabado y el servidor Web está listo para enviar una respuesta
        if (c == '\n' && currentLineIsBlank) {
 
            // Enviamos al cliente una respuesta HTTP
            client.println("HTTP/1.1 200 OK");
            client.println("Content-Type: text/html");
            client.println();
            //Página web en formato HTML
            client.println("<html><head></head>");
            client.println("<body>");
            client.println("<h1 align='center'>Los honguitos!!!</h1><h3 align='center'>Servidor Web con Arduino</h3>");
            //Creamos los botones. Para enviar parametres a través de HTML se utiliza el metodo URL encode. Los parámetros se envian a través del símbolo '?'
            client.println("<div style='text-align:center;'>");
            client.println("<button onClick=location.href='./?LED=ON\' style='margin:auto;background-color: #84B1FF;color: snow;padding: 10px;border: 1px solid #3F7CFF;width:65px;'>ON</button>");
            client.println("<button onClick=location.href='./?LED=OFF\' style='margin:auto;background-color: #84B1FF;color: snow;padding: 10px;border: 1px solid #3F7CFF;width:65px;'>OFF</button>");
            client.println("<br><button onClick=location.href='./?prende=ON\' style='margin:auto;background-color: #84B1FF;color: snow;padding: 10px;border: 1px solid #3F7CFF;width:65px;'>");
            client.println("ON");
            client.println("</button>");
            client.println("<button onClick=location.href='./?prende=OFF\' style='margin:auto;background-color: #84B1FF;color: snow;padding: 10px;border: 1px solid #3F7CFF;width:65px;'>");
            client.println("OFF");
            client.println("</button>");
            client.println("<br /><br />");
            client.println("<b>LED = ");
            client.print(estado);
            client.println("</b><br />");
            client.println("<b>LED1 = ");
            client.print(estado1);
            client.println("</b><br />");
            client.println("</p>valor de Temperatura");
            client.println(temperatura);
            client.println("</p>");
            client.print("Min 0 C <progress max=\"50\" value=\"");
            client.print(temperatura);
            client.print("\" ></progress>Max 50 C");
             client.println("</p>valor de la humedad");
            client.println(humedad);
            client.println("</p>");
            client.print("Min 0 C <progress max=\"50\" value=\"");
            client.print(humedad);
            client.print("\" ></progress>Max 50 C");
            client.println("</b></body>");
            client.println("</html>");
            break;
        }
        if (c == '\n') {
          currentLineIsBlank = true;
        }
        else if (c != '\r') {
          currentLineIsBlank = false;
        }
      }
    }
    //Dar tiempo al navegador para recibir los datos
    delay(1);
    client.stop();// Cierra la conexión
  }  
}
  void sensar(){
  int chk = DHT.read11(DHT11_PIN);
  if(DHT.temperature-4 <0){
  digitalWrite(A0,LOW);
  temperatura=0;
  humedad=0;
  }else{
   digitalWrite(A0,HIGH);
  temperatura=DHT.temperature-4;
  humedad=DHT.humidity;
  }
 }
